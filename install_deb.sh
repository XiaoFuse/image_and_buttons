#!/bin/bash

gcc main.c -o image_and_buttons_deb/usr/bin/image_and_buttons `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`
cp -R entered_text messages image header entries buttons 0.png start.sh image_and_buttons_deb/etc/image_and_buttons/default_config
rm image_and_buttons_deb/etc/image_and_buttons/default_config/placeholder
rm image_and_buttons_deb/usr/bin/placeholder

PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin

chmod -R 0775 image_and_buttons_deb
dpkg-deb --build image_and_buttons_deb
dpkg -i image_and_buttons_deb.deb

exit