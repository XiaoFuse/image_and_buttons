#!/bin/bash

FILE=($(zenity --file-selection --title "Pick a file"))
echo $FILE
if [[ $FILE == "" ]]; then
	exit
fi
convert $FILE 0.png
if [[ $? -eq 0 ]]; then
	for i in $FILE; do
  		if [ `identify "$i" | wc -l` -gt 1 ] ; then
			PWD= { pwd }
    		zenity --info --text "The file is most likely an animation, please select one of the files [0-N.png] at $PWD"
  		fi
	done
else
	notify-send "could not import $FILE"
fi

./buttons/button0.sh

exit