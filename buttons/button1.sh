#!/bin/bash

#get the coordintes
pos2="$(sed '2q;d' entered_text/1.txt)"
pos1="$(sed '1q;d' entered_text/1.txt)"

if [[ "$pos1" == "" ]]; then
	pos1="0 0"
fi
if [[ "$pos2" == "" ]]; then
	pos2="0 0"
fi

x1="${pos1%% *}"
y1="${pos1##* }"
x2="${pos2%% *}"
y2="${pos2##* }"

#if the coordintes were left blank or make no sense, flop the hole image
if [[ "$x1" == "$x2" ]] || [[ "$y1" == "$y2" ]]; then
	mogrify -flop  1.png
else
#else flop the selected area
	if [[ "$x2" -gt "$x1" ]]; then
		from_x=$x1
		x=$(($x2 - $x1))
	else
		from_x=$x2
		x=$(($x1 - $x2))
	fi
	
	if [[ "$y2" -gt "$y1" ]]; then
		from_y=$y1
		y=$(($y2 - $y1))
	else
		from_y=$y2
		y=$(($y1 - $y2))
	fi

	mogrify -region "$x"x"$y"+$from_x+$from_y -alpha on -background None  -flop 1.png
fi

exit