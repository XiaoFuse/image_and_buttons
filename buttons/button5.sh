#!/bin/bash
pos2="$(sed '2q;d' entered_text/1.txt)"
pos1="$(sed '1q;d' entered_text/1.txt)"

x1="${pos1%% *}"
y1="${pos1##* }"
x2="${pos2%% *}"
y2="${pos2##* }"
echo $pos1 | cut -d" " -f2
echo "x1: $x1"
echo "y1: $y1"
echo "x2: $x2"
echo "y2: $y2"

convert 2.png -matte -channel A +level 0,00% +channel 2.png

case "$pos2" in
	"") convert 1.png -resize "$x1" 1.png #change x and keep proportions
		#creating transparent 2.png with a max. width and height of 300
		ow="$(identify -format '%w' 1.png)"
		oh="$(identify -format '%h' 1.png)"
		if [ $ow -gt $oh ]; then
			w=300
			h=$(bc <<<"scale=0; 300 * $oh / $ow")
		else
			h=300
			w=$(bc <<<"scale=0; 300 * $ow / $oh")
		fi
		convert -size "$w"x"$h" canvas:transparent 2.png
		new_y="$(identify -format \"%h\" 1.png)"
		notify-send "resized (with keeping the proportions) to x:$x1 y:$new_y";;
	*)  convert 1.png -resize "$x1"x"$y2"\! 1.png #change x and y
		#creating transparent 2.png with a max. width and height of 300
		ow="$(identify -format '%w' 1.png)"
		oh="$(identify -format '%h' 1.png)"
		if [ $ow -gt $oh ]; then
			w=300
			h=$(bc <<<"scale=0; 300 * $oh / $ow")
		else
			h=300
			w=$(bc <<<"scale=0; 300 * $ow / $oh")
		fi
		convert -size "$w"x"$h" canvas:transparent 2.png
		notify-send "resized to x:$x1 y:$y2";;
esac

sed -i "1s/.*//" entered_text/1.txt
sed -i "2s/.*//" entered_text/1.txt

exit