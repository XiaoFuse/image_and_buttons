#!/bin/bash

cp 0.png 1.png

#creating transparent 2.png with a max. width and height of 300
ow="$(identify -format '%w' 1.png)"
oh="$(identify -format '%h' 1.png)"
if [ $ow -gt $oh ]; then
	w=300
	h=$(bc <<<"scale=0; 300 * $oh / $ow")
else
	h=300
	w=$(bc <<<"scale=0; 300 * $ow / $oh")
fi
convert -size "$w"x"$h" canvas:transparent 2.png

cp entered_text/0.txt entered_text/1.txt

exit