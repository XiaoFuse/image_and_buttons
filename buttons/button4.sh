#!/bin/bash

declare -A config_files
config_files=( ["b"]="buttons/button" ["h"]="header/button" ["e"]="entries/entry" )

command="$(sed '3q;d' entered_text/1.txt)"

case ${command%% *} in
	"gray")	convert 1.png -colorspace Gray 1.png ;;
	"red")  convert 1.png +level-colors ,Red 1.png;;
	"green")  convert 1.png +level-colors ,Green 1.png;;
	"blue")  convert 1.png +level-colors ,Blue 1.png;;
	"c+")	convert 1.png -contrast 10 1.png ;;
	"c-")	convert 1.png -contrast -10 1.png ;;
	"b+")	convert 1.png -brightness-contrast 5x0 1.png ;;
	"b-")	convert 1.png -brightness-contrast -5x0 1.png ;;
	"bc+")	convert 1.png -brightness-contrast 2x10 1.png ;;
	"bc-")	convert 1.png -brightness-contrast -2x-10 1.png ;;
	"invert") convert 1.png -level 100%,0 1.png ;;
	"sepia") convert 1.png  -color-matrix ' 0.393 0.769 0.189
											0.349 0.686 0.168
                                			0.272 0.534 0.131  ' 1.png ;;
	"polaroid") convert 1.png  -color-matrix '6x3:  1.438 -0.122 -0.016  0 0 -0.03
                  								-0.062  1.378 -0.016  0 0  0.05
                 								-0.062 -0.122 1.483   0 0 -0.02 ' 1.png ;;
	"s+") convert 1.png  -modulate 100,120 1.png ;;
	"s-") convert 1.png  -modulate 100,80 1.png ;;
	"help") zenity --info --width 200 --text="$(< messages/help.txt)" ;;
	"config") 	letter=${command:7:1} #letter after config
				echo "letter: $letter"
				number=${command:8} #everything after letter
				echo "number: $number"
				echo "xdg-open ${config_files[$letter]}$number.sh"
				xdg-open ${config_files[$letter]}$number.sh ;;
	"add") 	FILE=${command:4} #everything after add
			if [ ! -f $FILE ]; then
				notify-send "could not find $path"
				FILE=($(zenity --file-selection --title "Pick a file"))
			elif [[ $FILE = "" ]]; then
				FILE=($(zenity --file-selection --title "Pick a file"))				
			fi
			echo $FILE
			pos2="$(sed '2q;d' entered_text/1.txt)"
			pos1="$(sed '1q;d' entered_text/1.txt)"
			case "$pos1" in
				"") pos1="0 0"
			esac
			x1="${pos1%% *}"
			y1="${pos1##* }"
			x2="${pos2%% *}"
			y2="${pos2##* }"
			case "$pos2" in
				"") convert 1.png \( $FILE -background none -gravity center \) -gravity northwest -geometry +${x1}+${y1} -composite 1.png ;;
				*)	w=$(($x1 - $x2))
					if [ $x1 -gt $x2 ] ; then
						x=$x2
						w=$(($x1 - $x2))
					else
						x=$x1
						w=$(($x2 - $x1))
					fi
					if [ $y1 -gt $y2 ] ; then
						y=$y2
						h=$(($y1 - $y2))
					else
						y=$y1
						h=$(($y2 - $y1))
					fi
					convert 1.png \( $FILE -resize ${w}x${h} -background none -gravity center -extent ${w}x${h} \) -gravity northwest -geometry +${x}+${y} -composite 1.png ;;
			esac
			;;
	"clear") #creating transparent 2.png with a max. width and height of 300
			ow="$(identify -format '%w' 1.png)"
			oh="$(identify -format '%h' 1.png)"
			if [ $ow -gt $oh ]; then
				w=300
				h=$(bc <<<"scale=0; 300 * $oh / $ow")
			else
				h=300
				w=$(bc <<<"scale=0; 300 * $ow / $oh")
			fi
			convert -size "$w"x"$h" canvas:transparent 2.png
			cp entered_text/0.txt entered_text/1.txt ;;
	"")		#creating transparent 2.png with a max. width and height of 300
			ow="$(identify -format '%w' 1.png)"
			oh="$(identify -format '%h' 1.png)"
			if [ $ow -gt $oh ]; then
				w=300
				h=$(bc <<<"scale=0; 300 * $oh / $ow")
			else
				h=300
				w=$(bc <<<"scale=0; 300 * $ow / $oh")
			fi
			convert -size "$w"x"$h" canvas:transparent 2.png
			cp entered_text/0.txt entered_text/1.txt ;;
esac

exit