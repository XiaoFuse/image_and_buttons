#!/bin/bash
ow="$(identify -format '%w' 1.png)"
oh="$(identify -format '%h' 1.png)"
w="$(identify -format '%w' 2.png)"
h="$(identify -format '%h' 2.png)"

ox="$(($1))"
oy="$(($2))"
x=$(bc <<<"scale=0; $ox * $w / $ow")
y=$(bc <<<"scale=0; $oy * $h / $oh")
echo "1 $x"
echo "2 $y"

pos2="$(sed '2q;d' entered_text/1.txt)"
pos1="$(sed '1q;d' entered_text/1.txt)"
echo "pos2: $pos2"
echo "pos1: $pos1"

case "$pos1"  in

	"") convert 2.png -stroke lightgreen -strokewidth 1 -draw "line   $x,0 $x,$h" -stroke green -strokewidth 1 -draw "line   0,$y $w,$y" 2.png
		sed -i "1s/.*/$ox $oy/" entered_text/1.txt ;;

	*)  case "$pos2"  in
			"") convert 2.png -stroke green -strokewidth 1 -draw "line   $x,0 $x,$h" -stroke lightgreen -strokewidth 1 -draw "line   0,$y $w,$y" 2.png
				sed -i "2s/.*/$ox $oy/" entered_text/1.txt ;;
			*)	#creating transparent 2.png and with a max. width and height of 300
				ow="$(identify -format '%w' 1.png)"
				oh="$(identify -format '%h' 1.png)"
				if [ $ow -gt $oh ]; then
					w=300
					h=$(bc <<<"scale=0; 300 * $oh / $ow")
				else
					h=300
					w=$(bc <<<"scale=0; 300 * $ow / $oh")
				fi
				convert -size "$w"x"$h" canvas:transparent 2.png
				convert 2.png -stroke lightgreen -strokewidth 1 -draw "line   $x,0 $x,$h" -stroke green -strokewidth 1 -draw "line   0,$y $w,$y" 2.png
				sed -i "1s/.*/$ox $oy/" entered_text/1.txt
				sed -i "2s/.*//" entered_text/1.txt ;;
		esac ;;

esac

exit