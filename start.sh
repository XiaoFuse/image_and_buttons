#!/bin/bash
cp meassages/unknown_error.txt messages/temp_message.txt
cp entered_text/0.txt entered_text/1.txt
./buttons/button0.sh
rm 0-*.png

commands="zenity mogrify convert"
not_found=""


for command in ${commands};
do
	command -v $command >/dev/null 2>&1 || {
		echo >&2 "command not found: $command" && not_found="$not_found $command";
	}
done;

case "$not_found" in
	"") : ;; #do nothing
	*)	cp messages/dependences.txt messages/temp_message.txt
		sed -i "1a\ ${not_found}\\" messages/temp_message.txt
		echo "ERROR: One or more commands not found"
    	exit 1 # terminate and indicate error
esac

exit

#check for missing files and directorys with [ ! -f /tmp/foo.txt ] and [ ! -d /tmp/foo ]